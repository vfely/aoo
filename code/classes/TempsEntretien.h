#ifndef TEMPSENTRETIEN_H
#define TEMPSENTRETIEN_H

#include <string>
class Enclos;
class Agent;
class TempsEntretien {
	private :	
		Enclos *enclosTempsEntretien;
		Agent *agentTempsEntretien;
		int valeurTempsEntretien;
		
	public :
		TempsEntretien(Enclos *enclos = NULL, Agent *agent = NULL, int valeur = 0);
		
		std::string getEnclos ();
		int getValeur ();
		
		void setEnclos (Enclos *enclos);
		void setValeur(int valeur);
};
#endif
