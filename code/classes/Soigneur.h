#ifndef SOIGNEUR_H
#define SOIGNEUR_H

#include <string>
#include <list>
#include "Race.h"
#include "Animal.h"
#include "Personne.h"
#include "TempsSoin.h"
class Personne;
class TempsSoin;
class Soigneur : public Personne {
		std::list<Race *> liste_races;
		std::list<Race *> :: iterator It_races;
		std::list<TempsSoin *> liste_animaux;
		std::list<TempsSoin *> :: iterator It_animaux;
		
	public :
		void listeAnimaux();
		void listeCharge();
		void listeRaces();
		int getTempsSoinAnimal (std::string nomAnimal);
		
		void ajoutRace(Race *race);
		void ajoutAnimal(Animal *animal, int valeurTempsSoin);
		void affiche ();
		
		bool estHabilite(Animal *animal);
		
		
};
#endif
