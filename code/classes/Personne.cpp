#include "Personne.h"
#include <string> 
#include <iostream>

using namespace std;

// Constructeur
Personne :: Personne(string nom, string prenom, string adresse, int tarifHoraire) {
	nomPersonne = nom;
	prenomPersonne = prenom;
	adressePersonne = adresse;
	tarifHorairePersonne = tarifHoraire;	
}

// Getters
string Personne :: getNom () {
	return nomPersonne;
}

string Personne :: getPrenom () {
	return prenomPersonne;
}

string Personne :: getAdresse () {
	return adressePersonne;
}

int Personne :: getTarifHoraire () {
	return tarifHorairePersonne;
}

// Setters
void Personne :: setNom (string nom){
	nomPersonne = nom;
}

void Personne :: setPrenom (string prenom){
	prenomPersonne = prenom;
}

void Personne :: setAdresse (string adresse){
	adressePersonne = adresse;
}

void Personne :: setTarifHoraire (int tarifHoraire){
	tarifHorairePersonne = tarifHoraire;
}

void Personne :: affiche ()const {
	cout << nomPersonne << " \t| " << prenomPersonne << " \t| ";
}







