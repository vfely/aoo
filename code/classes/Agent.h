#ifndef AGENT_H
#define AGENT_H

#include "Personne.h"
#include "Enclos.h"
#include "TempsEntretien.h"
#include <string>
#include <list>


class TempsEntretien;
class Enclos;
class Personne;
class Agent : public Personne {
	private :
		std::list<TempsEntretien *> liste_enclos;
		std::list<TempsEntretien*> :: iterator It_enclos;
		
	public :
		void listeCharge();
		int getTempsEntretienEnclos(std::string nomEnclos);
		
		void ajoutEnclos (Enclos *enclos, int valeurTempsEntretien);
		
		void  affiche ();
};

#endif
