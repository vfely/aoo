#include "Soigneur.h"
#include <iostream>
using namespace std;

// Getters
void Soigneur :: listeAnimaux () {
	for(It_animaux = liste_animaux.begin(); It_animaux!= liste_animaux.end(); It_animaux++)
	{
		cout << (*It_animaux)->getAnimal() << ", ";
	}
}

void Soigneur :: listeCharge () {
	for(It_animaux = liste_animaux.begin(); It_animaux!= liste_animaux.end(); It_animaux++)
	{
		cout << " * " << (*It_animaux)->getAnimal() << " : " << (*It_animaux)->getValeur() << "h" << endl;
	}
}

void Soigneur :: listeRaces () {
	for(It_races = liste_races.begin(); It_races!= liste_races.end(); It_races++)
	{
		cout << (*It_races)->getNom() << ", ";
	}
}

int Soigneur :: getTempsSoinAnimal (string nomAnimal) {
	for(It_animaux = liste_animaux.begin(); It_animaux!= liste_animaux.end(); It_animaux++)
	{
		if ((*It_animaux)->getAnimal() == nomAnimal) {
			return (*It_animaux)->getValeur();
		}
	}
}

// Setters
void Soigneur :: ajoutRace(Race *race){
	liste_races.push_back(race);		
}

void Soigneur :: ajoutAnimal(Animal *animal, int valeurTempsSoin) {
	bool existe = false;
	for(It_animaux = liste_animaux.begin(); It_animaux!= liste_animaux.end(); It_animaux++)
	{
		if ((*It_animaux)->getAnimal() == animal->getNom()){
			cout<<"Ce soigneur s'occupe déja de "<<animal->getNom();
			existe = true;
		}
	}
	if(existe==false){
		TempsSoin *tempsSoin;
		tempsSoin = new TempsSoin;
		tempsSoin->setAnimal(animal);
		tempsSoin->setValeur(valeurTempsSoin);
	
		liste_animaux.push_back(tempsSoin);
	}
		
}

void Soigneur:: affiche (){
	Personne :: affiche();
	cout<<"soigneur"<<"\t | ";
	Soigneur :: listeAnimaux ();
}

bool Soigneur :: estHabilite (Animal *animal) {
	for(It_races = liste_races.begin(); It_races!= liste_races.end(); It_races++) {
		if (animal->getRace() == (*It_races)->getNom())
			return true;
	}
	
	return false;
}
