#include "Quantite.h"
#include "Race.h"
#include "Nourriture.h"
using namespace std;

// Constructeur
Quantite :: Quantite(Race *raceQ, Nourriture *nourritureQ, int adulte , int enfant )
{
	race=raceQ;
	nourriture=nourritureQ;
	quantiteAdulte=adulte;
	quantiteEnfant=enfant;
}

string Quantite ::  getRace ()
{
	return race->getNom();
}

string Quantite ::  getNomNourriture ()
{
	return nourriture->getType();
}

Nourriture* Quantite ::  getNourriture ()
{
	return nourriture;
}

int Quantite :: getAdulte ()
{
	return quantiteAdulte;
}
int Quantite ::  getEnfant ()
{
	return quantiteEnfant;
}

void Quantite :: setRace (Race *raceQ)
{
	race=raceQ;
}
void Quantite :: setNourriture (Nourriture *nourritureQ)
{
	nourriture=nourritureQ;
}
void Quantite :: setAdulte(int Qadulte)
{	
	quantiteAdulte=Qadulte;	
}
void Quantite :: setEnfant(int Qenfant)
{
	quantiteEnfant=Qenfant;	
}
