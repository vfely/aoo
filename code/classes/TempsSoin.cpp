#include "TempsSoin.h"
#include "Soigneur.h"
#include "Animal.h"

using namespace std;

//constructeur
TempsSoin :: TempsSoin(Animal *animal, Soigneur *soigneur, int valeur) {
	animalTempsSoin = animal;
	soigneurTempsSoin = soigneur;
	valeurTempsSoin = valeur;	
}

// Getters
string TempsSoin :: getAnimal ()
{
	return animalTempsSoin->getNom();
}

int TempsSoin :: getValeur ()
{
	return valeurTempsSoin;
}

// Setters
void TempsSoin :: setAnimal (Animal *animal)
{
	animalTempsSoin = animal;
}

void TempsSoin :: setValeur (int valeur)
{
	valeurTempsSoin = valeur;
}
