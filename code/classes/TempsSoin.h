#ifndef TEMPSSOIN_H
#define TEMPSSOIN_H

#include <string>
class Animal;
class Soigneur;
class TempsSoin {
	private :	
		Animal *animalTempsSoin;
		Soigneur *soigneurTempsSoin;
		int valeurTempsSoin;

	public :
		TempsSoin(Animal *animal = NULL, Soigneur *soigneur = NULL, int valeur = 0);
		
		std::string getAnimal ();
		int getValeur ();
		
		void setAnimal (Animal *animal);
		void setValeur(int valeur);
};
#endif
