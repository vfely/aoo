#ifndef RACE_H
#define RACE_H

#include <string>
#include <list>
class Quantite;
class Nourriture;
class Race {
		
		std::string nomRace;
		int ageMinimalAdulteRace;
		std ::list<Quantite *> liste_quantite; 
	
	public :
		Race (std::string nom = " ", int ageMinimalAdulte = 0);
		~Race();
		
		
		std::string getNom ();
		int getAgeMinimalAdulte ();
		void listeQuantite() ;
		void listeNourriture(int ageAnimal);
		float getCoutNourriture(int ageAnimal);
		
		
		void setNom (std::string nom);
		void setAgeMinimalAdulte (int ageMinimalAdulte);
		
		void ajoutNourriture(Nourriture *nourriture, int quantiteEnfant, int quanteAdulte);
};
#endif




