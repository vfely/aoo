#include <iostream>
#include "Animal.h"
#include "Race.h"
#include "Soigneur.h"
#include "Nourriture.h"
#include "Enclos.h"
#include "Date.h"
using namespace std;

// Constructeur & Destructeur
Animal :: Animal (int num, string nom, int age) {
	nomAnimal = nom;
	numAnimal = num;
	ageAnimal = age;
}

Animal :: ~Animal () {
}

// Getters
int Animal :: getNum () { 
	return numAnimal; 
}

string Animal :: getNom () { 
	return nomAnimal;
}

int Animal :: getAge () { 
	return ageAnimal; 
}

string Animal :: getEnclos() {
	Date *derniere_date = liste_enclos.back();
	return derniere_date->getNomEnclos();
}

Date* Animal :: getDateEnclos() {
	return liste_enclos.back();
}

string Animal :: getRace() {
	return raceAnimal->getNom();
}

void Animal :: listeSoigneurs() {
	list <Soigneur*> :: iterator It_soigneurs;
	for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
	{
		cout << (*It_soigneurs)->getPrenom() << " " << (*It_soigneurs)->getNom() << ", ";
	}
}

void Animal :: listeNourritures() {
	raceAnimal->listeNourriture(ageAnimal);
}

void Animal :: listeEnclos() {
	list <Date*> :: iterator It_enclos;
	for(It_enclos = liste_enclos.begin(); It_enclos != liste_enclos.end(); It_enclos++)
	{
		cout << " * " << (*It_enclos)->getNomEnclos() << " du " << (*It_enclos)->getArrivee() << " au " << (*It_enclos)->getDepart() << endl;
	}
}

float Animal :: getCoutNourriture(){
	return raceAnimal->getCoutNourriture(ageAnimal);
}

float Animal :: getCoutEntretien() {
	float coutEntretien = 0;
	int nbAnimauxEnclos, tempsEntretien;
	
	Date *date;
	date = new Date;
	Enclos *enclos;
	enclos = new Enclos;
	
	date = liste_enclos.back();
	enclos = date->getEnclos();
	
	tempsEntretien = enclos->getCoutEntretien();
	nbAnimauxEnclos = enclos->getNombreAnimaux();
	coutEntretien = tempsEntretien/nbAnimauxEnclos;
	
	return coutEntretien;
}

float Animal :: getCoutSoin() {
	int coutSoin = 0;
	list <Soigneur *> :: iterator It_soigneurs;
	for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
	{
		coutSoin += (*It_soigneurs)->getTarifHoraire() * (*It_soigneurs)->getTempsSoinAnimal(nomAnimal);
	}
	
	return coutSoin;
}

// Setters
void Animal :: setNum (int num){
	numAnimal = num;
}

void Animal :: setNom (string nom){
	nomAnimal = nom;
}

void Animal :: setAge (int age){
	ageAnimal = age;
}

void Animal :: setRace(Race *race) {
	raceAnimal = race;
}

void Animal :: ajoutSoigneur(Soigneur *soigneur) {
	bool existe = false;
	list <Soigneur *> :: iterator It_soigneurs;
	for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs!= liste_soigneurs.end(); It_soigneurs++)
	{
		if ((*It_soigneurs)->getNom() == soigneur->getNom()) {
			cout<<soigneur->getNom()<<" s'occupe déja de cet animal"<<endl;
			existe = true;
		}
	}
	if(existe==false){
		liste_soigneurs.push_back(soigneur);
	}
	
}

void Animal :: ajoutEnclos (Enclos *enclos, int dateArrivee, int dateDepart) {
	Date *date;
	date = new Date;
	date->setEnclos(enclos);
	date->setArrivee(dateArrivee);
	date->setDepart(dateDepart);
	
	liste_enclos.push_back(date);
}


