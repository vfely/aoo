#ifndef ANIMAL_H
#define ANIMAL_H


#include <string>
#include <list>

class Enclos;
class Race;
class Nourriture;
class Soigneur;
class Date;
class Animal {
	private :
		std::string nomAnimal;
		int numAnimal;
		int ageAnimal;
		Race *raceAnimal;
		std::list <Soigneur *> liste_soigneurs;
		std::list <Date *> liste_enclos;
		
	public :
		Animal (int num = 0, std::string nom = " ", int age = 0) ;
		~Animal ();
		
		int getNum();
		std::string getNom ();
		int getAge();
		std::string getEnclos ();
		Date* getDateEnclos();
		std::string getRace();
		void listeSoigneurs();
		void listeNourritures();
		void listeEnclos();
		float getCoutNourriture();
		float getCoutEntretien();
		float getCoutSoin();
		
		void setNum (int num);
		void setNom (std::string nom);
		void setAge (int age);
		void setRace(Race *race);
		void ajoutSoigneur(Soigneur *soigneur);
		void ajoutEnclos(Enclos *enclos, int dateArrivee, int dateDepart);
};

#endif
