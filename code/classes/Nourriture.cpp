#include "Nourriture.h"

using namespace std;

// Constructeur
Nourriture :: Nourriture(string type, string designation,int prix, string fournisseur) {
	typeNourriture = type;
	designationNourriture = designation;
	prixNourriture = prix;
	fournisseurNourriture = fournisseur;
}

// Getters
string Nourriture :: getType () { 
	return typeNourriture; 
}

string Nourriture :: getDesignation () { 
	return designationNourriture;
}

int Nourriture :: getPrix () { 
	return prixNourriture; 
}

string Nourriture :: getFournisseur() {
	return fournisseurNourriture;
}

// Setters
void Nourriture :: setType (string type){
	typeNourriture = type;
}

void Nourriture :: setDesignation (string designation){
	designationNourriture = designation;
}

void Nourriture :: setPrix (int prix){
	prixNourriture = prix;
}

void Nourriture :: setFournisseur(string fournisseur) {
	fournisseurNourriture = fournisseur;
}
