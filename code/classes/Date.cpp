#include "Date.h"
#include "Enclos.h"
using namespace std;

// Constructeur
Date :: Date(Enclos *enclos, int arrivee, int depart) {
	enclosDate = enclos;
	arriveeDate = arrivee;
	departDate = depart;	
}

string Date :: getNomEnclos ()
{
	return enclosDate->getNom();
}

Enclos* Date :: getEnclos ()
{
	return enclosDate;
}

int Date :: getArrivee ()
{
	return arriveeDate;
}
int Date ::getDepart ()
{
	return departDate;
}

void Date :: setEnclos (Enclos *enclos)
{
	enclosDate=enclos;
}

void Date :: setArrivee(int dateArrivee)
{
	arriveeDate=dateArrivee;
}
void Date ::setDepart(int dateDepart)
{
	departDate=dateDepart;
}
