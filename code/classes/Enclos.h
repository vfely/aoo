#ifndef ENCLOS_H
#define ENCLOS_H

#include "Agent.h"
#include "Animal.h"
#include <string>
#include <list>

class Animal;
class Agent;
class Enclos {
	private :
		std::string nomEnclos;
		int surfaceEnclos;
		std::list<Animal *> liste_animaux_enclos;
		std::list<Agent *> liste_agents;
	
	public :
		Enclos(std::string nom = " ", int surface = 0);
		~Enclos();
		
		std::string getNom();
		int getSurface();
		int getNombreAnimaux();
		int getCoutEntretien();
		
		void listeAgents();
		void listeAnimaux();
		
		void setNom(std :: string nom);
		void setSurface(int surf);
		
		void ajoutAnimal(Animal *animal);
		void ajoutAgent(Agent *agent);
};

#endif
