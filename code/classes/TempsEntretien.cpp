#include "TempsEntretien.h"
#include "Animal.h"
#include "Agent.h"

using namespace std;

// Constructeur
TempsEntretien :: TempsEntretien(Enclos *enclos, Agent *agent, int valeur) {
	enclosTempsEntretien = enclos;
	agentTempsEntretien = agent;
	valeurTempsEntretien = valeur;	
}

// Getters
string TempsEntretien :: getEnclos ()
{
	return enclosTempsEntretien->getNom();
}

int TempsEntretien :: getValeur ()
{
	return valeurTempsEntretien;
}

// Setters
void TempsEntretien :: setEnclos (Enclos *enclos)
{
	enclosTempsEntretien = enclos;
}

void TempsEntretien :: setValeur (int valeur)
{
	valeurTempsEntretien = valeur;
}
