#include "Race.h"
#include "Quantite.h"
#include "Nourriture.h"
#include <iostream>

using namespace std;

// Constructeur & Destructeur
Race :: Race ( string nom, int ageMinimalAdulte) {
	
	nomRace = nom;
	ageMinimalAdulteRace = ageMinimalAdulte;
}

Race :: ~Race () {
}

// Getters


string Race :: getNom () {
	return nomRace;
}

int Race :: getAgeMinimalAdulte () {
	return ageMinimalAdulteRace;
}

void Race :: listeQuantite() {
	list <Quantite*> :: iterator It_quantite;
	for(It_quantite = liste_quantite.begin(); It_quantite != liste_quantite.end(); It_quantite++)
	{
		cout << (*It_quantite)->getNomNourriture() << " "<<(*It_quantite)->getAdulte()<<" g pour adulte "<<(*It_quantite)->getEnfant()<<" g par enfant ";
	}
}
void Race :: listeNourriture(int ageAnimal) {
	list <Quantite*> :: iterator It_quantite;
	for(It_quantite = liste_quantite.begin(); It_quantite != liste_quantite.end(); It_quantite++)
	{
		cout << " * " << (*It_quantite)->getNomNourriture() << " ";
		
		if (ageAnimal <= ageMinimalAdulteRace) // Si enfant
			cout << (*It_quantite)->getEnfant();
		else // Si adulte
			cout << (*It_quantite)->getAdulte();
			
		 cout << "kg" << endl;
	}
}
float Race :: getCoutNourriture(int ageAnimal) {
	int coutNourriture = 0;
	int quantiteNourriture = 0;
	
	list <Quantite*> :: iterator It_quantite;
	for(It_quantite = liste_quantite.begin(); It_quantite != liste_quantite.end(); It_quantite++)
	{
		if (ageAnimal <= ageMinimalAdulteRace) // Si enfant
			quantiteNourriture = (*It_quantite)->getEnfant();
		else // Si adulte
			quantiteNourriture = (*It_quantite)->getAdulte();
			
		coutNourriture += quantiteNourriture * (*It_quantite)->getNourriture()->getPrix();
	}
	return coutNourriture;
}
// Setters


void Race :: setNom (string nom) {
	nomRace = nom;
}

void Race :: setAgeMinimalAdulte (int ageMinimalAdulte) {
	ageMinimalAdulteRace = ageMinimalAdulte;
}

void Race :: ajoutNourriture(Nourriture *nourriture, int quantiteEnfant, int quantiteAdulte)
{
	bool existe = false;
	list <Quantite*> :: iterator It_quantite;
	for(It_quantite = liste_quantite.begin(); It_quantite != liste_quantite.end(); It_quantite++)
	{
		if ((*It_quantite)->getNomNourriture() == nourriture->getType()) {
			cout<<"Cette race mange déja de cette nourriture"<<endl;
			existe = true;
		}
	}
	if(existe==false){
		Quantite *quantite;
		quantite = new Quantite;
		quantite->setNourriture(nourriture);
		quantite->setEnfant(quantiteEnfant);
		quantite->setAdulte(quantiteAdulte);
		liste_quantite.push_back(quantite);
	}
}
