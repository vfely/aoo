#ifndef DATE_H
#define DATE_H

#include <string>

class Enclos;
class Date {
	private :
		Enclos *enclosDate;
		int arriveeDate;
		int departDate;

	public :
		Date(Enclos *enclos = NULL, int arrivee = 0, int depart = 0);
		std::string getNomEnclos ();
		Enclos* getEnclos ();
		int getArrivee ();
		int getDepart ();
		
		void setEnclos (Enclos *enclos);
		void setArrivee(int dateArrivee);
		void setDepart(int dateDepart);
};
#endif
