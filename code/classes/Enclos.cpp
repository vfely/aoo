#include "Enclos.h"
#include <iostream>

using namespace std;

// Constructeur & Destructeur
Enclos :: Enclos (string nom, int surface) {
	nomEnclos = nom;
	surfaceEnclos = surface;
}

Enclos :: ~Enclos () {
}
		
// Getters

string Enclos :: getNom() {
	return nomEnclos;
}

int Enclos :: getSurface() {
	return surfaceEnclos;
}

int Enclos :: getNombreAnimaux () {
	return liste_animaux_enclos.size();
}

int Enclos :: getCoutEntretien () {
	int coutEntretien = 0;
	list <Agent *> :: iterator It_agents;
	for(It_agents = liste_agents.begin(); It_agents != liste_agents.end(); It_agents++)
	{
		coutEntretien += (*It_agents)->getTarifHoraire() * (*It_agents)->getTempsEntretienEnclos(nomEnclos);
	}
	
	return coutEntretien;
}

// Setters

void Enclos :: setNom(string nom) {
	nomEnclos = nom;
}

void Enclos :: setSurface(int surf) {
	surfaceEnclos = surf;
}
void Enclos :: ajoutAnimal(Animal *animal) {
	liste_animaux_enclos.push_back(animal);
}
void Enclos :: listeAgents () {
	list <Agent *> :: iterator It_agents;
	for(It_agents = liste_agents.begin(); It_agents!= liste_agents.end(); It_agents++)
	{
		cout << (*It_agents)->getNom() << ", ";
	}
}
void Enclos :: listeAnimaux () {
	list <Animal *> :: iterator It_animaux;
	for(It_animaux = liste_animaux_enclos.begin(); It_animaux!= liste_animaux_enclos.end(); It_animaux++)
	{
		cout << (*It_animaux)->getNom() << ", ";
	}
}
void Enclos :: ajoutAgent(Agent *agent) {
	liste_agents.push_back(agent);
}
