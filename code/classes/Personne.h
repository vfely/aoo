#ifndef PERSONNE_H
#define PERSONNE_H

#include <string>

class Personne {
	private :
		std::string nomPersonne;
		std::string prenomPersonne;
		std::string adressePersonne;
		int tarifHorairePersonne;

	public :
		Personne(std::string nom = " ", std::string prenom = " ", std::string adresse = " ", int tarifHoraire=0);
		
		std::string getNom ();
		std::string getPrenom ();
		std::string getAdresse ();
		int getTarifHoraire ();
		
		void setNom (std::string nom);
		void setPrenom (std::string prenom);
		void setAdresse (std::string adresse);
		void setTarifHoraire (int tarifHoraire);
		
		void affiche ()const; 
		
		
};
#endif
