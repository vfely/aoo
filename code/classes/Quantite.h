#ifndef QUANTITE_H
#define QUANTITE_H

#include <string>

class Race;
class Nourriture;
class Quantite {
	private :	
		Race *race;
		Nourriture *nourriture;
		int quantiteAdulte;
		int quantiteEnfant;
	
	public :
		Quantite(Race *raceQ=NULL, Nourriture *nourritureQ=NULL, int adulte = 0, int enfant = 0);
		std::string getRace ();
		Nourriture* getNourriture ();
		std :: string getNomNourriture ();
		int getAdulte ();
		int getEnfant ();
		
		
		void setRace (Race *raceQ);
		void setNourriture (Nourriture *nourritureQ);
		void setAdulte(int Qadulte);
		void setEnfant(int Qenfant);
};
#endif
