#ifndef NOURRITURE_H
#define NOURRITURE_H

#include <string>

class Nourriture {
	private :
		std::string typeNourriture;
		std::string designationNourriture;
		int prixNourriture;
		std::string fournisseurNourriture;

	public :
		Nourriture(std::string type=" ",std::string designation=" ", int prix = 0, std::string fournisseur = " ");
		
		std :: string getType ();
		std ::string getDesignation ();
		int getPrix () ;
		std ::string getFournisseur() ;
		
		void setType (std ::string type);
		void setDesignation (std ::string designation);
		void setPrix (int prix);
		void setFournisseur(std ::string fournisseur);
		
};
#endif
