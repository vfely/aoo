#include "Agent.h"
#include <iostream>
using namespace std;

// Getters
void Agent :: listeCharge () {
	for(It_enclos = liste_enclos.begin(); It_enclos!= liste_enclos.end(); It_enclos++)
	{
		cout << (*It_enclos)->getEnclos() << " ";
	}
}

int Agent :: getTempsEntretienEnclos (string nomEnclos) {
	for(It_enclos = liste_enclos.begin(); It_enclos!= liste_enclos.end(); It_enclos++)
	{
		if ((*It_enclos)->getEnclos() == nomEnclos) {
			return (*It_enclos)->getValeur();
		}
	}
}

// Setters
void Agent :: ajoutEnclos (Enclos *enclos, int valeurTempsEntretien) {
	bool existe = false;
	
	for(It_enclos = liste_enclos.begin(); It_enclos!= liste_enclos.end(); It_enclos++)
	{
		if ((*It_enclos)->getEnclos() == enclos->getNom()) {
			cout<<"Cet agent s'occupe déja de cet enclos"<<endl;
			existe = true;
		}
	}
	if(existe==false){
		TempsEntretien *tempsEntretien;
		tempsEntretien = new TempsEntretien;
		tempsEntretien->setEnclos(enclos);
		tempsEntretien->setValeur(valeurTempsEntretien);

		liste_enclos.push_back(tempsEntretien);
	}
}

void  Agent ::affiche (){
	Personne :: affiche();
	cout<<"agent"<<"\t | ";
	Agent :: listeCharge ();
}
