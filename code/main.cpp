#include <iostream>
#include <list>

#include "main.h"

using namespace std;

//listes
list<Animal*> liste_animaux;
list<Animal*> :: iterator It_animaux;
list<Race*> liste_races;
list<Race*> :: iterator It_races;
list<Enclos*> liste_enclos;
list<Enclos*> :: iterator It_enclos;
list<Agent*> liste_agents;
list<Agent*> :: iterator It_agents;
list<Soigneur*> liste_soigneurs;
list<Soigneur*> :: iterator It_soigneurs;
list<Nourriture*> liste_nourritures;
list<Nourriture*> :: iterator It_nourritures;

void clear()
{
	for (int i = 0; i < 50; i++) {
		cout << endl;
	}
}
//base de données
void chargerDonnees () {
	
	// Déclaration des races
	Race *zebre;
	zebre = new Race;
	Race *ours;
	ours = new Race;
	Race *singe;
	singe = new Race;
	
	// Déclaration des enclos
	Enclos *box1;
	box1 = new Enclos;
	Enclos *litiere1;
	litiere1 = new Enclos;
	Enclos *niche1;
	niche1 = new Enclos;
	Enclos *niche2;
	niche2 = new Enclos;
	
	// Déclaration des animaux
	Animal *jolyJumper;
	jolyJumper = new Animal;
	Animal *katnis;
	katnis = new Animal;
	Animal *minou;
	minou = new Animal;
	Animal *klebs;
	klebs = new Animal;
	
	// Déclaration des nourritures
	Nourriture *croquettes;
	croquettes = new Nourriture;
	Nourriture *viande;
	viande = new Nourriture;
	Nourriture *pain;
	pain = new Nourriture;
	
	// Déclaration des soigneurs
	Soigneur *Aaah;
	Aaah = new Soigneur;
	Soigneur *laura;
	laura = new Soigneur;
	Soigneur *brigitte;
	brigitte = new Soigneur;
	Soigneur *zelda;
	zelda = new Soigneur;
	Agent *nafissatou;
	nafissatou = new Agent;
	
	// Définition et ajouts des nourritures dans liste_nourritures
	croquettes->setType("croquettes");
	croquettes->setDesignation("croquettes");
	croquettes->setPrix(12);
	croquettes->setFournisseur("leclerc");
	
	liste_nourritures.push_back(croquettes);
	
	viande->setType("viande");
	viande->setDesignation("boeuf");
	viande->setPrix(15);
	viande->setFournisseur("carrefour");
	
	liste_nourritures.push_back(viande);
	
	pain->setType("pain");
	pain->setDesignation("baguette");
	pain->setPrix(2);
	pain->setFournisseur("carrefour");
	
	liste_nourritures.push_back(pain);
	// Définition et ajouts des races dans liste_races
	zebre->setNom("zebre");
	zebre->setAgeMinimalAdulte(5);
	zebre->ajoutNourriture(viande, 5, 100);
	zebre->ajoutNourriture(pain, 5, 10);
			
	liste_races.push_back(zebre);
	
	ours->setNom("ours");
	ours->setAgeMinimalAdulte(2);
	ours->ajoutNourriture(croquettes, 5, 100);
	
	liste_races.push_back(ours);
	
	singe->setNom("singe");
	singe->setAgeMinimalAdulte(5);
	singe->ajoutNourriture(viande, 5, 100);
	
	liste_races.push_back(singe);
	
	// Définition et ajouts des enclos à liste_enclos
	box1->setNom("box1");
	box1->setSurface(10);
	box1->ajoutAgent(nafissatou);
	
	liste_enclos.push_back(box1);
			
	litiere1->setNom("litiere1");
	litiere1->setSurface(2);
	litiere1->ajoutAgent(nafissatou);
	
	liste_enclos.push_back(litiere1);
	
	niche1->setNom("niche1");
	niche1->setSurface(3);
	niche1->ajoutAgent(nafissatou);
	
	liste_enclos.push_back(niche1);
	
	niche2->setNom("niche2");
	niche2->setSurface(10);
	niche2->ajoutAgent(nafissatou);
	
	liste_enclos.push_back(niche2);
	
	// Définitions et ajouts des animaux à liste_animaux
	jolyJumper->setNum(liste_animaux.size()+1);
	jolyJumper->setNom("JolyJumper");
	jolyJumper->setAge(5);
	jolyJumper->setRace(zebre);
	jolyJumper->ajoutSoigneur(zelda);
	jolyJumper->ajoutSoigneur(brigitte);
	jolyJumper->ajoutSoigneur(Aaah);
	jolyJumper->ajoutEnclos(box1, time(0), 0);
	box1->ajoutAnimal(jolyJumper);
	
	liste_animaux.push_back(jolyJumper);
	
	katnis->setNum(liste_animaux.size()+1);
	katnis->setNom("Katnis");
	katnis->setAge(1);
	katnis->setRace(ours);
	katnis->ajoutSoigneur(laura);
	katnis->ajoutEnclos(litiere1, time(0), 0);
	litiere1->ajoutAnimal(katnis);
	
	liste_animaux.push_back(katnis);
	
	minou->setNum(liste_animaux.size()+1);
	minou->setNom("minou");
	minou->setAge(3);
	minou->setRace(ours);
	minou->ajoutSoigneur(laura);
	minou->ajoutEnclos(litiere1, time(0), 0);
	litiere1->ajoutAnimal(minou);
	
	liste_animaux.push_back(minou);
			
	klebs->setNum(liste_animaux.size()+1);
	klebs->setNom("Klebs");
	klebs->setAge(14);
	klebs->setRace(singe);
	klebs->ajoutSoigneur(laura);
	klebs->ajoutEnclos(niche1, time(0) - 1000, time(0));
	klebs->ajoutEnclos(niche2, time(0), 0);
	niche2->ajoutAnimal(klebs);
	
	liste_animaux.push_back(klebs);
	
	// Personnes
	Aaah->setPrenom("Aaah");
	Aaah->setNom("Atchoum");
	Aaah->ajoutRace(zebre); // Peut soigner des zebres
	Aaah->ajoutRace(ours); // des ours
	Aaah->ajoutAnimal(jolyJumper,10); 
		
	liste_soigneurs.push_back(Aaah);
	
		
	laura->setPrenom("Laura");
	laura->setNom("Princesse");
	laura->setTarifHoraire(10);
	laura->ajoutRace(zebre); // Peut soigner des zebres
	laura->ajoutRace(ours); // des ours
	laura->ajoutRace(singe);
	laura->ajoutAnimal(katnis,10);
	laura->ajoutAnimal(minou,10);
	laura->ajoutAnimal(klebs,10);
		
	liste_soigneurs.push_back(laura);
	
		
	brigitte->setPrenom("Brigitte");
	brigitte->setNom("Bardot");
	brigitte->setTarifHoraire(8);
	brigitte->ajoutRace(zebre); // Peut soigner des zebres
	brigitte->ajoutRace(ours); // des ours
	brigitte->ajoutAnimal(jolyJumper,10); 
		
	liste_soigneurs.push_back(brigitte);
	
				
	zelda->setPrenom("Zelda");
	zelda->setNom("Casanova");
	zelda->setTarifHoraire(9);
	zelda->ajoutRace(zebre); // Peut soigner des zebres
	zelda->ajoutRace(ours); // des ours
	zelda->ajoutAnimal(jolyJumper,10); 
		
	liste_soigneurs.push_back(zelda);
	
	
	nafissatou->setPrenom("Nafissatou");
	nafissatou->setNom("Diallo");
	nafissatou->setTarifHoraire(3);
	nafissatou->ajoutEnclos(litiere1,10); // temps d'entretien 10h
	nafissatou->ajoutEnclos(box1,10); // idem
	nafissatou->ajoutEnclos(niche1,10); // idem
	nafissatou->ajoutEnclos(niche2,10); // idem
	
	liste_agents.push_back(nafissatou);
		
		
	cout << "Les données ont été chargées" << endl;
}
//fonction de tri sur les numéros des animaux
bool MonComparateurAnimal(Animal *A1, Animal *A2)
{
	return(A1->getNum()<A2->getNum()); 
}
// fonction qui affiche la liste des animaux
void listeAnimaux ()
{
	clear();
	cout << " --- Liste des animaux --- " << endl;
	cout << endl;
	
	// Tri par numéros d'animaux
	
	// Pour chaque animal :
	// * enclos
	// * soigneur
	// * cout en nourriture
	// * cout en entretien et soin
	
	int nb_animaux = liste_animaux.size();
	int choix = 0;
	int total_nourriture = 0, total_personnel = 0;
	liste_animaux.sort(MonComparateurAnimal);
	do {
		cout << " # | NOM \t| RACE \t| ENCLOS \t| SOIGNEUR \t| COUT NOURRITURE \t| COUT ENTRETIEN \t | COUT SOIN" << endl;
		for(It_animaux = liste_animaux.begin(); It_animaux != liste_animaux.end(); It_animaux++)
		{
			total_nourriture += (*It_animaux)->getCoutNourriture(); 
			total_personnel += (*It_animaux)->getCoutEntretien() + (*It_animaux)->getCoutSoin();
			
			cout << " " << (*It_animaux)->getNum();
			cout << " | " << (*It_animaux)->getNom();
			cout << " \t| " << (*It_animaux)->getRace();
			cout << " \t| " << (*It_animaux)->getEnclos();
			cout << " \t| "; (*It_animaux)->listeSoigneurs();
			cout << " \t| " << (*It_animaux)->getCoutNourriture() << "€";
			cout << " \t| " << (*It_animaux)->getCoutEntretien() << "€";
			cout << " \t| " << (*It_animaux)->getCoutSoin() << "€";
			cout << endl;
		}
	
		cout << endl;
		cout << "-------Cout journalier du zoo--------"<<endl;
		cout << endl;
		cout << "Cout en nourriture : " << total_nourriture << " € "<<endl;
		cout << "Cout en personnel : " << total_personnel << " € "<<endl;
		cout << endl;
		cout << "Saisir le numéro de l'animal à modifier." << endl;
		cout << "[0] Pour retourner au menu." << endl;
		cout << "Choix : ";
		cin >> choix;
	} while (choix < 0 && choix > nb_animaux);
	
	if (choix > 0) { // Choix d'un animal, sinon sortie
		for(It_animaux = liste_animaux.begin(); It_animaux != liste_animaux.end(); It_animaux++)
		{
			if (choix == (*It_animaux)->getNum())
				break;
		}
		
		modifierAnimal (*It_animaux);
	}
}
// fonction pour comparer deux soigneurs par rapport au nom de famille puis au prénom
bool MonComparateurS(Soigneur *S1, Soigneur *S2)
{
	if(S1->getNom()<S2->getNom()) return true;
	else
		if(S2->getNom()<S1->getNom()) return false;
	
		else return(S1->getPrenom()<S2->getPrenom());
	
}
// fonction pour comparer deux agents par rapport au nom de famille puis au prénom
bool MonComparateurA(Agent *A1, Agent *A2)
{
	if(A1->getNom()<A2->getNom()) return true;
	else
		if(A2->getNom()<A1->getNom()) return false;
	
		else return(A1->getPrenom()<A2->getPrenom());
	
}
//fonction qui affiche la liste du personnel
void listePersonnel ()
{
	clear();
	cout << " -------------------- LISTE DU PERSONNEL ------------------- " << endl;
	cout << endl << endl;
	cout << " --- Liste des soigneurs --- " << endl;
	cout << endl;
	
	// Tri sur nom puis prenom
	
	// Pour chaque personne :
	// * fonction
	// * animal a charge / enclos a charge
	
	//tri
	liste_soigneurs.sort(MonComparateurS); 
	liste_agents.sort(MonComparateurA); 
	
	int nb_soigneurs = liste_soigneurs.size();
	int nb_agents = liste_agents.size();
	
	int choix = 0;
	int i = 1;
	do {
		cout << " # | NOM \t| PRENOM \t| FONCTION \t| EN CHARGE" << endl;
		i = 1;
		for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
		{
			cout << " " << i << " | ";
			(*It_soigneurs)->affiche();
			cout << endl;
			i++;
		}
		cout << endl << " --- Liste des agents --- " << endl;
		cout << endl;
		cout << " # | NOM \t| PRENOM \t| FONCTION \t| EN CHARGE" << endl;
		for(It_agents = liste_agents.begin(); It_agents != liste_agents.end(); It_agents++)
		{
			cout << " " << i << " | " ;
			(*It_agents)->affiche();
			cout << endl;
			i++;
		}
		cout << endl;
		cout << "Saisir le numéro de la personne à modifier." << endl;
		cout << "[0] Pour retourner au menu." << endl;
		cout << "Choix : ";
		cin >> choix;
	} while (choix < 0 && choix > nb_agents+nb_soigneurs);
	if (choix > 0 && choix<=nb_soigneurs) { // Choix d'un soigneur, sinon sortie
		i = 1;
		for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
		{
			if (choix == i)
				break;
			i++;
		}
		
		modifierSoigneur (*It_soigneurs);
	}
	if (choix > nb_soigneurs) { // Choix d'un agent, sinon sortie
		choix=choix-nb_soigneurs;
		i = 1;
		for(It_agents = liste_agents.begin(); It_agents != liste_agents.end(); It_agents++)
		{
			if (choix == i)
				break;
			i++;
		}
		
		modifierAgent (*It_agents);
	}
	
}	
// fonction qui affiche la liste de nourriture
void listeNourriture ()
{
	clear();
	cout << " -------------------- LISTE DES NOURRITURES ------------------- " << endl;
	cout << endl << endl;
	
	int i = 1;
	
	cout << " # | TYPE \t| DESIGNATION \t| PRIX \t| FOURNISSEUR" << endl;
	i = 1;
	for(It_nourritures = liste_nourritures.begin(); It_nourritures != liste_nourritures.end(); It_nourritures++)
	{
		cout << " " << i <<" ";
		cout << "\t| " << (*It_nourritures)->getType() ;
		cout << "\t| " << (*It_nourritures)->getDesignation() ;
		cout << "\t| " << (*It_nourritures)->getPrix() ;
		cout << "\t| " << (*It_nourritures)->getFournisseur();
		cout << endl;
		i++;
	}
}
//fonction qui affiche la liste des enclos
void listeEnclos ()
{
	clear();
	cout << " -------------------- LISTE DES ENCLOS ------------------- " << endl;
	cout << endl << endl;
	
	int i = 1;
	
	cout << " # | NOM \t| SURFACE \t| AGENT \t| ANIMAUX" << endl;
	i = 1;
	for(It_enclos = liste_enclos.begin(); It_enclos != liste_enclos.end(); It_enclos++)
	{
		cout << " " << i <<" ";
		cout << "\t| " << (*It_enclos)->getNom() ;
		cout << "\t| " << (*It_enclos)->getSurface() ;
		cout << "\t| " ; (*It_enclos)->listeAgents() ;
		cout << "\t| " ; (*It_enclos)->listeAnimaux() ;
		cout << endl;
		i++;
	}
}

bool personneExiste (string nom, string prenom) {
	//test si le nom et le prenom existent dans liste agent
		for(It_agents = liste_agents.begin(); It_agents != liste_agents.end(); It_agents++)
		{
			if ((*It_agents)->getNom()==nom && (*It_agents)->getPrenom()==prenom)
			{
				return true;
			}
		}
		
		//test si le nom et le prenom existent dans liste soigneur
		for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
		{
			if ((*It_soigneurs)->getNom()==nom && (*It_soigneurs)->getPrenom()==prenom)
			{
				return true;
			}
		}
		
	return false;
}

//fonction pour ajouter un agent
void ajoutAgent ()
{
	Agent *agent;
	agent = new Agent;
	clear();
	cout << " --- Ajout d'un agent d'entretien --- " << endl;
	cout << endl;

	bool existe;
	do{
		existe = false;
		string nom;
		cout << "Saisir le nom : ";
		cin >> nom;
		agent->setNom(nom);

		string prenom;
		cout << "Saisir le prénom : ";
		cin >> prenom;
		agent->setPrenom(prenom);

		existe = personneExiste(nom,prenom);
		if (existe) {
			cout << "Cette personne existe déjà" << endl;
		}
	}while(existe);
	string adresse;
	cout << "Saisir l'adresse : ";
	cin >> adresse;
	agent->setAdresse(adresse);

	int tarifHoraire;
	cout << "Saisir le tarif horaire : ";
	cin >> tarifHoraire;
	agent->setTarifHoraire(tarifHoraire);

	cout << "Agent d'entretien " << agent->getPrenom() << " " << agent->getNom() << " ajouté" << endl;
	liste_agents.push_back(agent);
}
//fonction qui ajoute un soigneur
void ajoutSoigneur ()
{
	Soigneur *soigneur;
	soigneur = new Soigneur;
	clear();
	cout << " --- Ajout d'un soigneur --- " << endl;
	cout << endl;
	bool existe;
	do{
		existe = false;
		string nom;
		cout << "Saisir le nom : ";
		cin >> nom;
		soigneur->setNom(nom);
	
		string prenom;
		cout << "Saisir le prénom : ";
		cin >> prenom;
		soigneur->setPrenom(prenom);
		
		existe = personneExiste(nom,prenom);
		if (existe) {
			cout << "Cette personne existe déjà" << endl;
		}
	}while(existe);
	string adresse;
	cout << "Saisir l'adresse : ";
	cin >> adresse;
	soigneur->setAdresse(adresse);

	int tarifHoraire;
	cout << "Saisir le tarif horaire : ";
	cin >> tarifHoraire;
	soigneur->setTarifHoraire(tarifHoraire);
	
	/* --- Choix des habilitations --- */
	char reponse;
	reponse = 'o';
	do {
		Race *race = choixRace();
		soigneur->ajoutRace(race);
	
		cout << "Ajouter une autre race à ses habilitations ? o/n : ";
		cin >> reponse;
	
	} while(reponse == 'o');

	cout << "Soigneur " << soigneur->getPrenom() << " " << soigneur->getNom() << " ajoutée" << endl;
	liste_soigneurs.push_back(soigneur);
}
//fonction pour ajouter une race
void ajoutRace()
{	
	
	Race *race;
	race = new Race;
	cout << endl;
	cout << " --- Ajout d'une race --- " << endl;
	cout << endl;
	
	// Choix du nom
	bool existe;
	do{
		existe = false;
		string nom;
		cout << "Saisir le nom de la race : ";
		cin >> nom;
		race->setNom(nom);
		//test si cette race existe
		for(It_races = liste_races.begin(); It_races != liste_races.end(); It_races++)
		{
			if ((*It_races)->getNom()==nom)
			{
				cout<<"Race existante "<<endl;
				existe = true;
			}
		}
	}while(existe);
	// Choix de l'age minimal pour un adulte de cette race
	int ageMinimalAdulte;
	cout << "Saisir l'age minimal d'un(e) " << race->getNom() << " pour etre adulte : ";
	cin >> ageMinimalAdulte;
	race->setAgeMinimalAdulte(ageMinimalAdulte);

	/* --- Choix de la nourriture --- */
	char reponse;
	reponse = 'o';
	int quantite_adulte,quantite_enfant;
	do {
		Nourriture *nourriture = choixNourriture();
		cout << "Quantité pour un adulte : ";
		cin >> quantite_adulte;
		cout << "Quantité pour un enfant : ";
		cin >> quantite_enfant;
	
		race->ajoutNourriture(nourriture, quantite_enfant, quantite_adulte);
	
		cout << "Affecter une autre nourriture à cette race ? o/n : ";
		cin >> reponse;
	
	} while(reponse == 'o');

	cout << "Race " << race->getNom() << " ajoutée" << endl;
	liste_races.push_back(race);
}
//fonction pour ajouter un enclos
void ajoutEnclos ()
{
	Enclos *enclos;
	enclos = new Enclos;
	cout << endl;
	cout << " --- Ajout d'un enclos --- " << endl;
	cout << endl;
	
	cout << endl;
	bool existe;
	do{
		existe = false;
		string nom;
		cout << "Saisir le nom de l'enclos : ";
		cin >> nom;
		enclos->setNom(nom);
		//test si cet enclos existe
		for(It_enclos = liste_enclos.begin(); It_enclos != liste_enclos.end(); It_enclos++)
		{
			if ((*It_enclos)->getNom()==nom)
			{
				cout<<"Enclos existant "<<endl;
				existe = true;
			}
		}
	}while(existe);
	int surface;
	cout << "Saisir la surface de l'enclos (en m2) : ";
	cin >> surface;
	enclos->setSurface(surface);
	cout << "Enclos " << enclos->getNom() << " ajouté" << endl;
	liste_enclos.push_back(enclos);
}
//fonction pour ajouter de la nourriture
void ajoutNourriture ()
{
	Nourriture *nourriture;
	nourriture = new Nourriture;
	cout << endl;
	cout << " --- Ajout d'une nourriture --- " << endl;
	cout << endl;
	bool existe;
	do{
		existe = false;
		string type;
		cout << "Saisir le type de nourriture : ";
		cin >> type;
		nourriture->setType(type);
		//test si cette nourriture existe
		for(It_nourritures = liste_nourritures.begin(); It_nourritures != liste_nourritures.end(); It_nourritures++)
		{
			if ((*It_nourritures)->getType()==type)
			{
				cout<<"Nourriture existante "<<endl;
				existe = true;
			}
		}
	}while(existe);
	string designation;
	cout << "Saisir sa designation : ";
	cin >> designation;
	nourriture->setDesignation(designation);

	int prix;
	cout << "Saisir son prix : ";
	cin >> prix;
	nourriture->setPrix(prix);

	string fournisseur;
	cout << "Saisir son fournisseur : ";
	cin >> fournisseur;
	nourriture->setFournisseur(fournisseur);

	cout << "Nourriture " << nourriture->getType() << " ajouté" << endl;
	liste_nourritures.push_back(nourriture);
}
// fonction pour choisir une race dans la liste des races existantes
Race *choixRace() {
	int race = 0;
	int nb_races = liste_races.size();
	int i = 1;
	cout << "Choix de la race : " << endl;
	do {
		cout << "Races existantes : " << endl;

	
		i = 1;
		for(It_races = liste_races.begin(); It_races != liste_races.end(); It_races++)
		{
			cout << "[" << i << "] " << (*It_races)->getNom() <<" mange " ;
			(*It_races)->listeQuantite() ;
			cout<<endl;
			i++;
		}
	
		cout << "[" << nb_races+1 << "]" << " Nouvelle race ?" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> race;
	} while (race <= 0 || race > nb_races+1);

	if (race == nb_races+1) {
		ajoutRace();
	}
	
	// Retourne la race choisie
	i = 1;
	for(It_races = liste_races.begin(); It_races != liste_races.end(); It_races++)
	{
		if(i == race)
		{
			return *It_races;
		}
		i++;
		
	}
}
// fonction pour choisir un enclos dans la liste des enclos existants
Enclos *choixEnclos () {
	int enclos = 0;
	int nb_enclos = liste_enclos.size();
	int i = 1;
	cout << "Affectation de l'animal à un enclos" << endl;
	do {
		cout << "Enclos disponibles : " << endl;
		i = 1;
		for(It_enclos = liste_enclos.begin(); It_enclos != liste_enclos.end(); It_enclos++)
		{
			cout << "[" << i << "] " << (*It_enclos)->getNom() << endl;
			i++;
		}
		cout << "[" << nb_enclos+1 << "]" << " Nouvel enclos ?" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> enclos; 
	} while (enclos <= 0 || enclos > nb_enclos+1);

	if (enclos == nb_enclos+1) {
		ajoutEnclos();
	}
	
	
	// Retourne l'enclos choisi
	i = 1;
	for(It_enclos = liste_enclos.begin(); It_enclos != liste_enclos.end(); It_enclos++)
	{
		if(i == enclos)
		{
			return *It_enclos;
		}
		i++;
	}
}
// fonction pour choisir un soigneur dans la liste des soigneurs existants
Soigneur *choixSoigneur () {
	int soigneur = 0;
	int nb_soigneurs = liste_soigneurs.size();
	int i = 1;
	
	cout << "Affectation d'un soigneur à l'animal" << endl;
	do {
		cout << "Soigneurs disponibles : " << endl;
		i = 1;
		for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
		{
			cout << "[" << i << "] " << (*It_soigneurs)->getNom() << endl;
			i++;
		}
		cout << "[" << nb_soigneurs+1 << "]" << " Nouveau soigneur ?" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> soigneur; 
	} while (soigneur <= 0 || soigneur > nb_soigneurs+1);

	if (soigneur == nb_soigneurs+1) {
		ajoutSoigneur();
	}
	
	// Retourne le soigneur choisi
	i = 1;
	for(It_soigneurs = liste_soigneurs.begin(); It_soigneurs != liste_soigneurs.end(); It_soigneurs++)
	{
		if(i == soigneur)
		{
			return *It_soigneurs;
		}
		i++;
	}
}
// fonction pour choisir une nourriture dans la liste des nourritures existantes
Nourriture *choixNourriture () {
	int nourriture = 0;
	int nb_nourritures = liste_nourritures.size();
	int i = 1;
	do {
		cout << "Nourritures disponibles : " << endl;
		i = 1;
		for(It_nourritures = liste_nourritures.begin(); It_nourritures != liste_nourritures.end(); It_nourritures++)
		{
			cout << "[" << i << "] " << (*It_nourritures)->getType() << endl;
			i++;
		}
		cout << "[" << nb_nourritures+1 << "]" << " Nouvelle nourriture ?" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> nourriture; 
	} while (nourriture <= 0 || nourriture > nb_nourritures+1);

	if (nourriture == nb_nourritures+1) {
		ajoutNourriture();
	}
	

	// Retourne la nourriture choisie
	i = 1;
	for(It_nourritures = liste_nourritures.begin(); It_nourritures != liste_nourritures.end(); It_nourritures++)
	{
		if(i == nourriture)
		{
			return *It_nourritures;
		}
		i++;
	}
}
// fonction pour choisir un animal dans la liste des animaux existants
Animal *choixAnimal () {
	int animal = 0;
	int nb_animaux = liste_animaux.size();
	
	do {
		cout << "Animaux disponibles : " << endl;
	
		for(It_animaux = liste_animaux.begin(); It_animaux != liste_animaux.end(); It_animaux++)
		{
			cout << "[" << (*It_animaux)->getNum() << "] " << (*It_animaux)->getNom() << endl;
		
		}
		cout << "[" << nb_animaux+1 << "]" << " Nouvel animal ?" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> animal; 
	} while (animal <= 0 || animal > nb_animaux+1);

	if (animal == nb_animaux+1) {
		ajoutAnimal();
	}
	

	// Retourner l'animal choisi 
	int i = 1;
	for(It_animaux = liste_animaux.begin(); It_animaux != liste_animaux.end(); It_animaux++)
	{
		if(i == animal)
		{
			return *It_animaux;
		}
		i++;
	}
}

// fonction pour ajouter un animal
void ajoutAnimal ()
{	
	clear();
	cout << " --- Ajout d'un animal --- " << endl;
	cout << endl;
	
	Animal *animal;
	animal = new Animal;
	
	animal->setNum(liste_animaux.size()+1);
	cout << "Ajout de l'animal #" << animal->getNum() << endl;
	cout << endl;
	
	/* --- Choix de la race de l'animal --- */

	Race *race ;
	race = new Race;
	race = choixRace();
	animal->setRace(race);
	
	/* --- Choix du nom de l'animal --- */
	bool existe;
	string nom;
	do{
		existe=false;
		cout << "Choix du nom de l'animal : ";
		cin >>  nom;
		//test si l'animal existe
		for(It_animaux = liste_animaux.begin(); It_animaux != liste_animaux.end(); It_animaux++)
		{
			if ((*It_animaux)->getNom()==nom)
			{
				cout<<"Animal existant "<<endl;
				existe = true;
			}
		}
	}while(existe);
	animal->setNom(nom);
	/* --- Choix de l'enclos --- */
	
	Enclos *enclos = choixEnclos();
	animal->ajoutEnclos(enclos, time(0), 0);
	enclos->ajoutAnimal(animal);
	
	/* --- Ajout de l'animal dans la liste --- */
	liste_animaux.push_back(animal);
	
	cout << "Un(e)" << animal->getRace();
	cout << " nommé(e) " << animal->getNom();
	cout << " a été ajouté(e) et affecté(e) à l'enclos " << animal->getEnclos() << endl; 
}
//fonction pour modifier un animal
void modifierAnimal (Animal *animal) {
	clear();
	cout << " --- Modification d'un animal --- " << endl;
	cout << endl;
	
	cout << animal->getNum() << " - " << animal->getNom() << endl;
	cout << endl;
	
	cout << "Liste des nourritures avec les quantités journalières : " << endl;
	animal->listeNourritures();
	cout << endl;
	
	cout << "Liste des enclos occupés :" << endl;
	animal->listeEnclos();
	cout << endl;
	
	char choix;
	cout << "Le changer d'enclos ? o/n : ";
	cin >> choix;
	
	if (choix == 'o') {
		Enclos *enclos = choixEnclos();
		
		// Ajout date depart et nouvel enclos
		Date *date_depart;
		date_depart = new Date;
		date_depart = animal->getDateEnclos();
		date_depart->setDepart(time(0));
		int dep = date_depart->getDepart();
		cout<<"date depart "<<dep;
		
		animal->ajoutEnclos(enclos,dep , 0);
		enclos->ajoutAnimal(animal);
	}
}
//fonction pour modifier un soigneur
void modifierSoigneur (Soigneur *soigneur) {
	clear();
	cout << " --- Modification d'un soigneur --- " << endl;
	cout << endl;
	
	cout << "Prénom : " << soigneur->getPrenom() << endl;
	cout << "Nom : " << soigneur->getNom() << endl;
	cout << "Adresse : " << soigneur->getAdresse() << endl;
	cout << "Tarif horaire : " << soigneur->getTarifHoraire() << " €/heure" << endl;
	cout << endl;
	cout << "Habilité à soigner les races suivantes : ";
	soigneur->listeRaces();
	cout << endl;
	cout << "Charge du soigneur : " << endl;
	soigneur->listeCharge();
	cout << endl;
	
	char choix;
	cout << "Modifier sa charge ? o/n : ";
	cin >> choix;
	
	if (choix == 'o') {
		int estHabilite;
		do {
			estHabilite = false;
			Animal *animal = choixAnimal();
			if (soigneur->estHabilite(animal)) {
				int tempsSoin;
				cout << "Temps nécessaire au soin de cet animal : ";
				cin >> tempsSoin;
				animal->ajoutSoigneur(soigneur);
				soigneur->ajoutAnimal(animal,tempsSoin);
				estHabilite = true;
			}
		
			else {
				cout << soigneur->getPrenom() << " n'est pas habilité à soigner un " << animal->getRace() << endl;
				cout << endl;
			}
		} while (!estHabilite);
	}
	
	
}
//fonction pour modifier un agent
void modifierAgent (Agent *agent) {
	clear();
	cout << " --- Modification d'un agent --- " << endl;
	cout << endl;
	
	cout << "Prénom : " << agent->getPrenom() << endl;
	cout << "Nom : " << agent->getNom() << endl;
	cout << "Adresse : " << agent->getAdresse() << endl;
	cout << "Tarif horaire : " << agent->getTarifHoraire() << " €/heure" << endl;
	cout << endl;
	cout << "Charge de l'agent : ";
	agent->listeCharge();
	cout << endl;
	
	char choix;
	cout << "Changer sa charge ? o/n : ";
	cin >> choix;
	
	if (choix == 'o') {
		Enclos *enclos = choixEnclos();
		
		int tempsEntretien;
		cout << "Temps nécessaire à l'entretien de cet enclos : ";
		cin >> tempsEntretien;
		
		agent->ajoutEnclos(enclos,tempsEntretien);
		enclos->ajoutAgent(agent);
				
	}
}
//programme principal
int main()
{	
	chargerDonnees();
	
	int choix = 0;
	
	do {
		clear();          
		cout << " ,-------.                     ,--.     ,--.,--.,--.   ,--. " << endl;
		cout << " `--.   /  ,---.  ,---.  ,---. |  ,---. `--'|  | \\  `.'  /  " << endl;
		cout << "   /   /  | .-. || .-. || .-. ||  .-.  |,--.|  |  .'    \\   " << endl;
		cout << "  /   `--.' '-' '' '-' '| '-' '|  | |  ||  ||  | /  .'.  \\  " << endl;
		cout << " `-------' `---'  `---' |  |-' `--' `--'`--'`--''--'   '--' " << endl;
		cout << "                        `--'                                " << endl;
		cout << "   _______________________________________ " << endl;
		cout << " / Enfin un logiciel pour les amoureux des \\ " << endl;
		cout << " | zoo !                                   | " << endl;
		cout << " \\ Une prouesse de Sirella L-B & Vincent F / " << endl;
		cout << "   --------------------------------------- " << endl;
		cout << "        \\   ^__^ " << endl;
		cout << "         \\  (oo)\\_______ " << endl;
		cout << "            (__)\\       )\\/\\ " << endl;
		cout << "                ||----w | " << endl;
		cout << "                ||     || " << endl;
		cout << endl;
		cout << endl;
		cout << "[1] Liste des animaux" << endl;
		cout << "[2] Liste du personnel" << endl;
		cout << "[3] Liste des nourritures" << endl;
		cout << "[4] Liste des enclos" << endl;
		cout << endl;
		cout << "[5] Ajouter un animal" << endl;
		cout << "[6] Ajouter un agent" << endl;
		cout << "[7] Ajouter un soigneur" << endl;
		cout << "[8] Ajouter un enclos" << endl;
		cout << "[9] Ajouter une race" << endl;
		cout << endl;
		cout << "[0] Quitter" << endl;
		cout << endl;
		cout << "Choix : ";
		cin >> choix;
		
		
		int retour;
		switch (choix)
		{	
			case 1 :
				listeAnimaux();
			break;

			case 2 :
				listePersonnel();
			break;
			
			case 3 :
				listeNourriture ();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 4 :
				listeEnclos ();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 5 :
				ajoutAnimal();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;

			case 6 :
				ajoutAgent();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 7 :
				ajoutSoigneur();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 8 :
				ajoutEnclos();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 9 :
				ajoutRace();
				
				cout << endl;
				cout << "[0] Pour retourner au menu : ";
				cin >> retour;
			break;
			
			case 0 :
				cout << "Bye !" << endl;
			break;
		}
	} while(choix > 0 && choix <= 9);
}
