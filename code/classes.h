#ifndef CLASSES_H
#define CLASSES_H

#include "classes/Animal.h"
#include "classes/Race.h"
#include "classes/Enclos.h"
#include "classes/Agent.h"
#include "classes/Soigneur.h"
#include "classes/Personne.h"
#include "classes/Nourriture.h"
#include "classes/Date.h"
#include "classes/TempsSoin.h"
#include "classes/TempsEntretien.h"
#include "classes/Quantite.h"

#endif
