#include "classes.h"

void clear();
void chargerDonnees();

void listeAnimaux();
void listePersonnel();
void listeNourriture();

Animal *choixAnimal();
Nourriture *choixNourriture();
Enclos *choixEnclos();
Soigneur *choixSoigneur();
Race *choixRace();

void ajoutAnimal();
void ajoutAgent();
void ajoutSoigneur();
void ajoutRace();
void ajoutEnclos();

void modifierAnimal(Animal *animal);
void modifierSoigneur(Soigneur *soigneur);
void modifierAgent(Agent *agent);

bool personneExiste (std::string nom, std::string prenom);
